package xygx.printer.entity.order;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 外卖订单信息
 * 
 * @author herui
 *
 */
public class OrderTakeout implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 配送费
     */
    private BigDecimal shippingFee;

    /**
     * 餐盒费
     */
    private BigDecimal mealFee;

    /**
     * 配送时间
     */
    private LocalDateTime deliveryTime;

    /**
     * 返回 配送费
     *
     * @return 配送费
     */
    public BigDecimal getShippingFee() {
        return shippingFee;
    }

    /**
     * 设置配送费
     *
     * @param shippingFee
     *            配送费
     */
    public void setShippingFee(
            BigDecimal shippingFee) {
        this.shippingFee = shippingFee;
    }

    /**
     * 返回 餐盒费
     *
     * @return 餐盒费
     */
    public BigDecimal getMealFee() {
        return mealFee;
    }

    /**
     * 设置餐盒费
     *
     * @param mealFee
     *            餐盒费
     */
    public void setMealFee(
            BigDecimal mealFee) {
        this.mealFee = mealFee;
    }

    /**
     * 返回 配送时间
     *
     * @return 配送时间
     */
    public LocalDateTime getDeliveryTime() {
        return deliveryTime;
    }

    /**
     * 设置配送时间
     *
     * @param deliveryTime
     *            配送时间
     */
    public void setDeliveryTime(
            LocalDateTime deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

}
