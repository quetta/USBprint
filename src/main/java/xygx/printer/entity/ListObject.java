package xygx.printer.entity;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

/**
 * 列表对象
 * 
 * @author guer
 *
 * @param <T>
 *            数据类型
 */
public class ListObject<T extends Serializable> implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 数据列表
     */
    private List<T> list = Collections.emptyList();

    /**
     * 数据总数
     */
    private int count;

    /**
     * 查询内容数量
     */
    private int pageSize = 10;

    /**
     * 返回数据列表
     *
     * @return 数据列表
     */
    public List<T> getList() {
        return list;
    }

    /**
     * 设置数据列表
     *
     * @param list
     *            数据列表
     */
    public void setList(
            List<T> list) {
        this.list = list;
    }

    /**
     * 返回数据总数
     *
     * @return 数据总数
     */
    public int getCount() {
        return count;
    }

    /**
     * 设置数据总数
     *
     * @param count
     *            数据总数
     */
    public void setCount(
            int count) {
        this.count = count;
    }

    /**
     * 返回分页尺寸
     *
     * @return 分页尺寸
     */
    public int getPageSize() {
        return pageSize;
    }

    /**
     * 设置分页尺寸
     *
     * @param pageSize
     *            分页尺寸
     */
    public void setPageSize(
            int pageSize) {
        this.pageSize = pageSize;
    }
}
