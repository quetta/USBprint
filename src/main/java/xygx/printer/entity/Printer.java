package xygx.printer.entity;

import java.io.Serializable;

import xygx.printer.helper.PageWidth;

public class Printer implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long printerId;

    private String deviceName;

    private String deviceId;

    private String deviceSecretKey;

    private String deviceRemark;

    private String printerType;

    private PageWidth printerPageType;

    private String printerStatus;

    private Long copies;

    public Long getPrinterId() {
        return printerId;
    }

    public void setPrinterId(
            Long printerId) {
        this.printerId = printerId;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(
            String deviceName) {
        this.deviceName = deviceName;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(
            String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceSecretKey() {
        return deviceSecretKey;
    }

    public void setDeviceSecretKey(
            String deviceSecretKey) {
        this.deviceSecretKey = deviceSecretKey;
    }

    public String getDeviceRemark() {
        return deviceRemark;
    }

    public void setDeviceRemark(
            String deviceRemark) {
        this.deviceRemark = deviceRemark;
    }

    public String getPrinterType() {
        return printerType;
    }

    public void setPrinterType(
            String printerType) {
        this.printerType = printerType;
    }

    public PageWidth getPrinterPageType() {
        return printerPageType;
    }

    public void setPrinterPageType(
            PageWidth printerPageType) {
        this.printerPageType = printerPageType;
    }

    public String getPrinterStatus() {
        return printerStatus;
    }

    public void setPrinterStatus(
            String printerStatus) {
        this.printerStatus = printerStatus;
    }

    public Long getCopies() {
        return copies;
    }

    public void setCopies(
            Long copies) {
        this.copies = copies;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Printer [printerId=");
        builder.append(printerId);
        builder.append(", deviceName=");
        builder.append(deviceName);
        builder.append(", deviceId=");
        builder.append(deviceId);
        builder.append(", deviceSecretKey=");
        builder.append(deviceSecretKey);
        builder.append(", deviceRemark=");
        builder.append(deviceRemark);
        builder.append(", printerType=");
        builder.append(printerType);
        builder.append(", printerPageType=");
        builder.append(printerPageType);
        builder.append(", printerStatus=");
        builder.append(printerStatus);
        builder.append(", copies=");
        builder.append(copies);
        builder.append("]");
        return builder.toString();
    }
}
