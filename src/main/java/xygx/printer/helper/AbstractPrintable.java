package xygx.printer.helper;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;

public abstract class AbstractPrintable implements Printable {

    private boolean isFirst = true;

    @Override
    public final int print(
            Graphics gra,
            PageFormat pf,
            int pageIndex) throws PrinterException {
        if (pageIndex != 0) {
            return NO_SUCH_PAGE;
        }
        if (isFirst) {
            isFirst = false;
            return PAGE_EXISTS;
        }

        gra.setColor(Color.black);

        print0((Graphics2D) gra, pf);

        return PAGE_EXISTS;
    }

    protected abstract void print0(
            Graphics2D gra,
            PageFormat pf);

}
