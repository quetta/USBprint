package xygx.printer.entity.order;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

/**
 * 订单
 * 
 * @author guer
 *
 */
public class Order implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 订单号
     */
    private String orderNum;

    /**
     * 店铺名称
     */
    private String shopName;

    /**
     * 订单支付时间
     */
    private LocalDateTime payTime;

    /**
     * 订单价格
     */
    private BigDecimal orderPrice;

    /**
     * 订单优惠价格
     */
    private BigDecimal favourablePrice;

    /**
     * 订单发票信息
     */
    private OrderInvoiceInfo orderInvoiceInfo;

    /**
     * 外卖订单信息
     */
    private OrderTakeout orderTakeout;

    /**
     * 返回 orderInvoiceInfo
     *
     * @return orderInvoiceInfo
     */
    public OrderInvoiceInfo getOrderInvoiceInfo() {
        return orderInvoiceInfo;
    }

    /**
     * 设置orderInvoiceInfo
     *
     * @param orderInvoiceInfo
     *            orderInvoiceInfo
     */
    public void setOrderInvoiceInfo(
            OrderInvoiceInfo orderInvoiceInfo) {
        this.orderInvoiceInfo = orderInvoiceInfo;
    }

    /**
     * 订单备注
     */
    private String orderContent;

    /**
     * 是否支付完成
     */
    private Boolean isFinishPay;

    /**
     * 订单商品
     */
    private List<OrderGoods> orderGoods = Collections.emptyList();

    /**
     * 订单联系人
     */
    private OrderContact orderContact;

    /**
     * 返回 orderNum
     *
     * @return orderNum
     */
    public String getOrderNum() {
        return orderNum;
    }

    /**
     * 设置orderNum
     *
     * @param orderNum
     *            orderNum
     */
    public void setOrderNum(
            String orderNum) {
        this.orderNum = orderNum;
    }

    /**
     * 返回 shopName
     *
     * @return shopName
     */
    public String getShopName() {
        return shopName;
    }

    /**
     * 设置shopName
     *
     * @param shopName
     *            shopName
     */
    public void setShopName(
            String shopName) {
        this.shopName = shopName;
    }

    /**
     * 返回 payTime
     *
     * @return payTime
     */
    public LocalDateTime getPayTime() {
        return payTime;
    }

    /**
     * 设置payTime
     *
     * @param payTime
     *            payTime
     */
    public void setPayTime(
            LocalDateTime payTime) {
        this.payTime = payTime;
    }

    /**
     * 返回 orderPrice
     *
     * @return orderPrice
     */
    public BigDecimal getOrderPrice() {
        return orderPrice;
    }

    /**
     * 设置orderPrice
     *
     * @param orderPrice
     *            orderPrice
     */
    public void setOrderPrice(
            BigDecimal orderPrice) {
        this.orderPrice = orderPrice;
    }

    /**
     * 返回 orderTakeout
     *
     * @return orderTakeout
     */
    public OrderTakeout getOrderTakeout() {
        return orderTakeout;
    }

    /**
     * 设置orderTakeout
     *
     * @param orderTakeout
     *            orderTakeout
     */
    public void setOrderTakeout(
            OrderTakeout orderTakeout) {
        this.orderTakeout = orderTakeout;
    }

    /**
     * 返回 orderContent
     *
     * @return orderContent
     */
    public String getOrderContent() {
        return orderContent;
    }

    /**
     * 设置orderContent
     *
     * @param orderContent
     *            orderContent
     */
    public void setOrderContent(
            String orderContent) {
        this.orderContent = orderContent;
    }

    /**
     * 返回 isFinishPay
     *
     * @return isFinishPay
     */
    public Boolean getIsFinishPay() {
        return isFinishPay;
    }

    /**
     * 设置isFinishPay
     *
     * @param isFinishPay
     *            isFinishPay
     */
    public void setIsFinishPay(
            Boolean isFinishPay) {
        this.isFinishPay = isFinishPay;
    }

    /**
     * 返回 orderGoods
     *
     * @return orderGoods
     */
    public List<OrderGoods> getOrderGoods() {
        return orderGoods;
    }

    /**
     * 设置orderGoods
     *
     * @param orderGoods
     *            orderGoods
     */
    public void setOrderGoods(
            List<OrderGoods> orderGoods) {
        this.orderGoods = orderGoods;
    }

    /**
     * 返回 orderContact
     *
     * @return orderContact
     */
    public OrderContact getOrderContact() {
        return orderContact;
    }

    /**
     * 设置orderContact
     *
     * @param orderContact
     *            orderContact
     */
    public void setOrderContact(
            OrderContact orderContact) {
        this.orderContact = orderContact;
    }

    /**
     * 返回 favourablePrice
     *
     * @return favourablePrice
     */
    public BigDecimal getFavourablePrice() {
        return favourablePrice;
    }

    /**
     * 设置favourablePrice
     *
     * @param favourablePrice
     *            favourablePrice
     */
    public void setFavourablePrice(
            BigDecimal favourablePrice) {
        this.favourablePrice = favourablePrice;
    }

}
