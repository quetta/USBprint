package xygx.printer.entity.order;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 订单联系人
 * 
 * @author guer
 *
 */
@ApiModel("订单联系人")
public class OrderContact implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 联系人
     */
    @ApiModelProperty("联系人")
    private String contactName;

    /**
     * 联系电话
     */
    @ApiModelProperty("联系电话")
    private String contactPhone;

    /**
     * 地址
     */
    @ApiModelProperty("地址")
    private String address;

    /**
     * 返回 contactName
     *
     * @return contactName
     */
    public String getContactName() {
        return contactName;
    }

    /**
     * 设置contactName
     *
     * @param contactName
     *            contactName
     */
    public void setContactName(
            String contactName) {
        this.contactName = contactName;
    }

    /**
     * 返回 contactPhone
     *
     * @return contactPhone
     */
    public String getContactPhone() {
        return contactPhone;
    }

    /**
     * 设置contactPhone
     *
     * @param contactPhone
     *            contactPhone
     */
    public void setContactPhone(
            String contactPhone) {
        this.contactPhone = contactPhone;
    }

    /**
     * 返回 address
     *
     * @return address
     */
    public String getAddress() {
        return address;
    }

    /**
     * 设置address
     *
     * @param address
     *            address
     */
    public void setAddress(
            String address) {
        this.address = address;
    }

}
