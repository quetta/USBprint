package xygx.printer.aliyun.iot;

import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.print.PageFormat;
import java.time.format.DateTimeFormatter;

import com.alibaba.fastjson.JSONObject;

import xygx.printer.entity.order.Order;
import xygx.printer.entity.order.OrderCopies;
import xygx.printer.entity.order.OrderGoods;
import xygx.printer.entity.order.OrderInvoiceInfo;
import xygx.printer.helper.AbstractPrintable;
import xygx.printer.helper.PrinterHelper;

public class PrintableImpl extends AbstractPrintable {

    private String data;

    private String fontName = "新宋体";

    private int mm = 58;

    public PrintableImpl(
            String data) {
        this.data = data;
    }

    @Override
    protected void print0(
            Graphics2D g,
            PageFormat pf) {
        OrderCopies orderCopies = JSONObject.parseObject(data, OrderCopies.class);
        Order order = orderCopies.getOrder();
        int x = 18;
        int y = 20;
        printWithFontAndStringCenter(g, 6, "***共享点餐***", y);

        y += 30;
        printWithFontAndStringCenter(g, 3, order.getShopName(), y);

        y += 30;
        String num = order.getOrderNum();
        num = num.substring(num.length() - 4, num.length());
        printWithFontAndStringCenter(g, 5, "#" + num, y);

        y += 30;
        printWithFontAndStringCenter(g, 6, "==已在线支付==", y);

        y += 20;
        printWithFontAndStringCenter(g, 3, "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~", y);

        y += 20;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String payTime = order.getPayTime().format(formatter);
        printWithFontAndStringCenter(g, 3, "下单时间：" + payTime, y);

        y += 10;
        String sendTime = order.getOrderTakeout().getDeliveryTime().format(formatter);
        printWithFontAndStringCenter(g, 3, "送达时间：" + sendTime, y);

        y += 15;
        for (OrderGoods orderGoods : order.getOrderGoods()) {
            g.setFont(getFont(Font.BOLD, PrinterHelper.getPoint(3)));

            int halfWidth = PrinterHelper.getPoint(mm) / 2;
            y = printWrap(halfWidth, g, y, x, orderGoods.getGoodsName() + "(" + orderGoods.getGoodsContent() + ")");

            printWithFontAndStringCenter(g, 3, "X" + orderGoods.getGoodsCount(), y);

            g.drawString(orderGoods.getAmount().toString(), (PrinterHelper.getPoint(mm) - x)
                    - g.getFontMetrics().stringWidth(orderGoods.getAmount().toString()), y);
            y += 15;

        }

        printWithFontAndStringCenter(g, 3, "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~", y);
        y += 10;
        g.drawString("优惠合计：", x, y);
        g.drawString("-" + order.getFavourablePrice().toString(), (PrinterHelper.getPoint(mm) - x)
                - g.getFontMetrics().stringWidth("-" + order.getFavourablePrice().toString()), y);
        y += 15;
        printWithFontAndStringCenter(g, 3, "~~~~~~~~~~~~~~~~~~~~其他费用~~~~~~~~~~~~~~~~~~~~", y);

        y += 10;

        g.setFont(getFont(Font.BOLD, PrinterHelper.getPoint(3)));
        g.drawString("餐盒费", x, y);
        String feemealString = order.getOrderTakeout().getMealFee().toString();
        g.drawString(feemealString, (PrinterHelper.getPoint(mm) - x) - g.getFontMetrics().stringWidth(feemealString),
                y);

        y += 10;
        g.setFont(getFont(Font.BOLD, PrinterHelper.getPoint(3)));
        g.drawString("配送费", x, y);
        String shippingFeeString = order.getOrderTakeout().getShippingFee().toString();
        g.drawString(shippingFeeString,
                (PrinterHelper.getPoint(mm) - x) - g.getFontMetrics().stringWidth(shippingFeeString), y);

        y += 10;
        printWithFontAndStringCenter(g, 3, "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~", y);

        y += 15;
        g.setFont(getFont(Font.BOLD, PrinterHelper.getPoint(5)));
        g.drawString("已付", x, y);
        String orderPriceString = order.getOrderPrice().toString();
        g.drawString(orderPriceString,
                (PrinterHelper.getPoint(mm) - x) - g.getFontMetrics().stringWidth(orderPriceString), y);

        String contentString = order.getOrderContent();
        g.setFont(getFont(Font.BOLD, PrinterHelper.getPoint(5)));
        if (contentString != null && !contentString.isEmpty()) {
            y += 10;
            printWithFontAndStringCenter(g, 3, "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~", y);
            y += 15;
            g.setFont(getFont(Font.BOLD, PrinterHelper.getPoint(5)));
            g.drawString("备注：", x, y);
            OrderInvoiceInfo invoiceInfo = orderCopies.getOrder().getOrderInvoiceInfo();
            if (invoiceInfo != null) {
                if (invoiceInfo.getTitle() != null && !invoiceInfo.getTitle().isEmpty()) {
                    g.drawString("需要发票", x + g.getFontMetrics().stringWidth("备注： "), y);
                }
            }
            y += 15;
            y = printWrap(PrinterHelper.getPoint(mm), g, y, x, contentString);
        }

        y += 15;
        printWithFontAndStringCenter(g, 3, "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~", y);

        y += 10;

        g.setFont(getFont(Font.BOLD, PrinterHelper.getPoint(5)));
        String addressString = order.getOrderContact().getAddress();
        y = printWrap(PrinterHelper.getPoint(mm), g, y, x, addressString);

        y += 15;
        g.drawString(order.getOrderContact().getContactName(), x, y);
        y += g.getFont().getSize() + 2;
        g.drawString(order.getOrderContact().getContactPhone(), x, y);

        y += 20;
        g.setFont(getFont(Font.BOLD, PrinterHelper.getPoint(3)));// 设置字体
        String orderString = "订单号：" + order.getOrderNum();
        g.drawString(orderString, getPoint(g, mm, orderString), y);

        y += 30;
        printWithFontAndStringCenter(g, 6, "***共享点餐***", y);
        y += 10;
        g.drawString("\n", x, y);
    }

    /**
     * 居中单行打印固定字符串
     * 
     * @param g
     *            打印对象
     * @param fontSize
     *            字体大小
     * @param string
     *            打印文字
     * @param y
     *            y坐标
     */
    private void printWithFontAndStringCenter(
            Graphics2D g,
            int fontSize,
            String string,
            int y) {
        g.setFont(getFont(Font.BOLD, PrinterHelper.getPoint(fontSize)));// 设置字体
        g.drawString(string, getPoint(g, mm, string), y);
    }

    /**
     * 换行打印
     * 
     * @param g
     *            打印对象
     * @param y
     *            y轴
     * @param x
     *            x轴
     * @param printString
     *            打印的字符串
     * @return y
     */
    private int printWrap(
            int rowWidth,
            Graphics2D g,
            int y,
            int x,
            String printString) {
        // 一行的最多容量
        int fullLen = (rowWidth - x) / g.getFont().getSize() - 1;
        // 字符串长度
        int titleLen = printString.length();
        if (titleLen > fullLen) {
            int writeLen = (printString.length() + fullLen - 1) / fullLen;// 可写入行数
            String string;
            for (int i = 0; i < writeLen - 1; i++) {
                int begin = i * fullLen;
                int end = begin + fullLen;
                string = printString.substring(begin, end);
                g.drawString(string, 18, y);// i+1
                y += g.getFont().getSize();
            }
            string = printString.substring((writeLen - 1) * fullLen);
            g.drawString(string, 18, y);// 最后一行
        } else {
            g.drawString(printString, x, y);
        }
        return y;
    }

    private Font getFont(
            int fontType,
            int size) {
        return new Font(fontName, fontType, size);

    }

    /**
     * 居中打印
     * 
     * @param g
     *            打印对象
     * @param mm
     *            纸张宽度
     * @param content
     *            打印文字
     * @return
     */
    private int getPoint(
            Graphics2D g,
            int mm,
            String content) {
        return (PrinterHelper.getPoint(mm) - g.getFontMetrics().stringWidth(content)) / 2;

    }

}
