package xygx.printer.entity;

import java.io.Serializable;

/**
 * 店铺
 * 
 * @author zhoukui
 *
 */
public class Shop implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 店铺ID
     */
    private Long shopId;

    /**
     * 店铺名称
     */
    private String shopName;

    /**
     * 返回 shopId
     *
     * @return shopId
     */
    public final Long getShopId() {
        return shopId;
    }

    /**
     * 设置shopId
     *
     * @param shopId
     *            shopId
     */
    public final void setShopId(
            Long shopId) {
        this.shopId = shopId;
    }

    /**
     * @return the shopName
     */
    public String getShopName() {
        return shopName;
    }

    /**
     * @param shopName
     *            the shopName to set
     */
    public void setShopName(
            String shopName) {
        this.shopName = shopName;
    }

}
