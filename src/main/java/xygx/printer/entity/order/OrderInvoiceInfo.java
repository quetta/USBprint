package xygx.printer.entity.order;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;

/**
 * 订单发票信息
 * 
 * @author guer
 *
 */
@ApiModel("订单发票信息")
public class OrderInvoiceInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 发票类型
     */
    private InvoiceType invoiceType;

    /**
     * 抬头
     */
    private String title;

    /**
     * 税号
     */
    private String ein;

    /**
     * 返回 invoiceType
     *
     * @return invoiceType
     */
    public InvoiceType getInvoiceType() {
        return invoiceType;
    }

    /**
     * 设置invoiceType
     *
     * @param invoiceType
     *            invoiceType
     */
    public void setInvoiceType(
            InvoiceType invoiceType) {
        this.invoiceType = invoiceType;
    }

    /**
     * 返回 title
     *
     * @return title
     */
    public String getTitle() {
        return title;
    }

    /**
     * 设置title
     *
     * @param title
     *            title
     */
    public void setTitle(
            String title) {
        this.title = title;
    }

    /**
     * 返回 ein
     *
     * @return ein
     */
    public String getEin() {
        return ein;
    }

    /**
     * 设置ein
     *
     * @param ein
     *            ein
     */
    public void setEin(
            String ein) {
        this.ein = ein;
    }

}
