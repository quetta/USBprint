package xygx.printer.entity.order;

/**
 * 发票类型
 * 
 * @author guer
 *
 */
public enum InvoiceType {

    /**
     * 公司
     */
    COMPANY,

    /**
     * 个人
     */
    INDIVIDUAL;
}
