package xygx.printer.layout;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.print.PrintService;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;

import okhttp3.Call;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Request.Builder;
import okhttp3.RequestBody;
import okhttp3.Response;
import xygx.printer.aliyun.config.AliyunConfig;
import xygx.printer.aliyun.iot.PrinterClient;
import xygx.printer.entity.ListObject;
import xygx.printer.entity.LoginResponse;
import xygx.printer.entity.Printer;
import xygx.printer.entity.Result;
import xygx.printer.entity.Shop;
import xygx.printer.helper.PrinterHelper;

@Component
public class LoginInterface extends JFrame {

    private static final long serialVersionUID = 1L;

    // private static final String BASE_URL = "http://127.0.0.1:8080/";

    private static final String BASE_URL = "http://api.test.gongxiangdiancan.com/";

    @Autowired
    private AliyunConfig aliyunConfig;

    private JPanel usernamePanel = new JPanel();

    private JTextField usernameInputFiled = new JTextField(13);

    private JLabel usernameTitleLabel = new JLabel("用户名");

    private JPanel passwordPanel = new JPanel();

    private JPasswordField passwordInputFiled = new JPasswordField(13);

    private JLabel passwordTitleLabel = new JLabel("验证码");

    private JPanel loginPanel = new JPanel();

    private JButton loginButton = new JButton("登陆");

    private JButton sendSmsButton = new JButton("发送验证码");

    private JPanel loginedPanel = new JPanel();

    private JLabel loginedLabel = new JLabel("登陆成功,已开始监听");

    private JLabel versionLabel = new JLabel("版本号：1.0.0");

    private String jwt;

    private JComboBox<ShopDisplayItem> box = new JComboBox<>();

    private JPanel shopPanel = new JPanel();

    private JLabel shopLabel = new JLabel("店铺列表");

    private JButton shopButton = new JButton("登录到店铺");

    @Autowired
    private OkHttpClient client;

    public LoginInterface() {
        initBase();
        initUsernamePanel();
        initPasswordPanel();
        initLoginPanel();
        initTest();
    }

    private void initTest() {
        usernamePanel.setVisible(true);
        passwordPanel.setVisible(true);
        loginPanel.setVisible(true);
        loginedPanel.add(loginedLabel);
        loginedPanel.add(versionLabel);
    }

    private void initBase() {
        setLayout(new GridLayout(5, 1));

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

        setLocation((screenSize.width - 400) / 2, (screenSize.height - 300) / 2);

        setSize(450, 300);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        setTitle("共享点餐打印机监听终端（版本：1.0.0）");
    }

    private void initUsernamePanel() {
        usernamePanel.add(usernameTitleLabel);
        usernamePanel.add(usernameInputFiled);
        add(usernamePanel);
    }

    private void initPasswordPanel() {
        passwordPanel.add(passwordTitleLabel);
        passwordPanel.add(passwordInputFiled);
        add(passwordPanel);
    }

    private void initLoginPanel() {
        loginPanel.add(sendSmsButton);
        loginPanel.add(loginButton);
        add(loginPanel);

        sendSmsButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(
                    ActionEvent e) {
                String username = usernameInputFiled.getText();

                if (StringUtils.isBlank(username)) {
                    JOptionPane.showMessageDialog(null, "请输入用户名", "发送验证码失败", JOptionPane.ERROR_MESSAGE);
                    return;
                }

                Builder builder = new Request.Builder();
                builder.url(BASE_URL + "commons/phoneCode/" + username.trim());
                builder.post(RequestBody.create(MediaType.parse("application/json"), ""));
                Request request = builder.build();
                Call call = client.newCall(request);

                String body = null;
                try {
                    Response response = call.execute();
                    body = response.body().string();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }

                if (body == null) {
                    JOptionPane.showMessageDialog(null, "链接服务器失败，请稍后再试", "发送验证码失败", JOptionPane.ERROR_MESSAGE);
                    return;
                }

                ParameterizedTypeReference<Result<Void>> typeRef = new ParameterizedTypeReference<Result<Void>>() {
                };

                Result<Void> result = JSON.parseObject(body, typeRef.getType());

                if (!result.isStatus()) {
                    JOptionPane.showMessageDialog(null, result.getMessage(), "发送验证码失败", JOptionPane.ERROR_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(null, "发送成功", "发送验证码成功", JOptionPane.INFORMATION_MESSAGE);
                }
            }
        });

        loginButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(
                    ActionEvent actionEvent) {
                String username = usernameInputFiled.getText();

                if (StringUtils.isBlank(username)) {
                    JOptionPane.showMessageDialog(null, "请输入用户名", "登录失败", JOptionPane.ERROR_MESSAGE);
                    return;
                }

                String password = String.valueOf(passwordInputFiled.getPassword());

                if (StringUtils.isBlank(password)) {
                    JOptionPane.showMessageDialog(null, "请输入验证码", "登录失败", JOptionPane.ERROR_MESSAGE);
                    return;
                }

                Map<String, String> data = new HashMap<>(2);
                data.put("sellerName", username);
                data.put("code", password);

                Builder builder = new Request.Builder();
                builder.url(BASE_URL + "seller/seller/loginByCode");
                builder.post(RequestBody.create(MediaType.parse("application/json"), JSON.toJSONString(data)));
                Request request = builder.build();
                Call call = client.newCall(request);

                String body = null;
                try {
                    Response response = call.execute();
                    body = response.body().string();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }

                if (body == null) {
                    JOptionPane.showMessageDialog(null, "链接服务器失败，请稍后再试", "发送验证码失败", JOptionPane.ERROR_MESSAGE);
                    return;
                }

                ParameterizedTypeReference<Result<LoginResponse>> typeRef = new ParameterizedTypeReference<Result<LoginResponse>>() {
                };

                Result<LoginResponse> result = JSON.parseObject(body, typeRef.getType());

                if (!result.isStatus()) {
                    JOptionPane.showMessageDialog(null, result.getMessage(), "发送验证码失败", JOptionPane.ERROR_MESSAGE);
                } else {
                    jwt = result.getData().getJwt();

                    usernamePanel.setVisible(false);
                    passwordPanel.setVisible(false);
                    loginPanel.setVisible(false);
                    loadShopList();

                }
            }
        });
    }

    public void loadShopList() {
        Builder builder = new Request.Builder();
        builder.addHeader("TOKEN", jwt);
        builder.url(BASE_URL + "/seller/shopList");
        builder.get();
        Request request = builder.build();
        Call call = client.newCall(request);

        String body = null;
        try {
            Response response = call.execute();
            body = response.body().string();
        } catch (Exception e2) {
            e2.printStackTrace();
        }

        if (body == null) {
            JOptionPane.showMessageDialog(null, "链接服务器失败，请稍后再试", "获取店铺列表失败", JOptionPane.ERROR_MESSAGE);
            return;
        }

        ParameterizedTypeReference<Result<List<Shop>>> typeRef = new ParameterizedTypeReference<Result<List<Shop>>>() {
        };

        Result<List<Shop>> result = JSON.parseObject(body, typeRef.getType());
        List<Shop> shops = result.getData();
        for (Shop shop : shops) {
            box.addItem(new ShopDisplayItem(shop));
        }
        box.setVisible(true);
        shopButton.setVisible(true);
        shopPanel.add(shopLabel);
        shopPanel.add(box);
        shopPanel.add(shopButton);
        shopPanel.setVisible(true);
        add(shopPanel);
        shopButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(
                    ActionEvent actionEvent) {
                Long shopId = shops.get(box.getSelectedIndex()).getShopId();
                if (shopId == null) {
                    JOptionPane.showMessageDialog(null, "店铺无效", "登录失败", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                shopPanel.setVisible(false);
                loginedPanel.setVisible(true);
                add(loginedPanel);
                loadPrinterList();
            }
        });

    }

    public static class ShopDisplayItem {
        Shop shop;

        ShopDisplayItem(
                Shop shop) {
            this.shop = shop;
        }

        @Override
        public String toString() {
            return shop == null ? "" : shop.getShopName();
        }

    }

    public void loadPrinterList() {
        Builder builder = new Request.Builder();
        builder.addHeader("TOKEN", jwt);
        builder.url(BASE_URL + "seller/printer?pageSize=" + Integer.MAX_VALUE);
        builder.get();
        Request request = builder.build();
        Call call = client.newCall(request);

        String body = null;
        try {
            Response response = call.execute();
            body = response.body().string();
        } catch (Exception e2) {
            e2.printStackTrace();
        }

        if (body == null) {
            JOptionPane.showMessageDialog(null, "链接服务器失败，请稍后再试", "获取打印机列表失败", JOptionPane.ERROR_MESSAGE);
            return;
        }

        aliyunConfig.setProductKey("XjvtaELjgfF");

        System.out.println(body);

        ParameterizedTypeReference<Result<ListObject<Printer>>> typeRef = new ParameterizedTypeReference<Result<ListObject<Printer>>>() {
        };

        Result<ListObject<Printer>> result = JSON.parseObject(body, typeRef.getType());

        if (!result.isStatus()) {
            JOptionPane.showMessageDialog(null, result.getMessage(), "发送验证码失败", JOptionPane.ERROR_MESSAGE);
        } else {
            result.getData().getList().parallelStream().forEach(this::initPrinter);
        }
    }

    private void initPrinter(
            Printer printer) {
        if (printer == null || !"USB".equals(printer.getPrinterType())) {
            return;
        }

        System.out.println("初始化打印机监听");
        System.out.println(printer.getDeviceId());
        System.out.println(printer.getDeviceSecretKey());

        PrintService printService = PrinterHelper.getLocalPrinterMap().get(printer.getDeviceName());

        if (printService == null) {
            System.out.println("未找到打印机:" + printer.getDeviceName());
            return;
        }

        new PrinterClient(printService, aliyunConfig.productKey, printer.getDeviceId(), printer.getDeviceSecretKey(),
                printer.getPrinterPageType()).start();
    }
}
