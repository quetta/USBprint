package xygx.printer.helper;

import java.awt.print.Book;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import java.awt.print.PrinterJob;
import java.util.HashMap;
import java.util.Map;

import javax.print.PrintService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.guerlab.commons.abstractslayout.ApplicationError;

public class PrinterHelper {

    private static final Logger LOGGER = LoggerFactory.getLogger(PrinterHelper.class);

    private PrinterHelper() {

    }

    public static void print(
            Printable printable,
            PageWidth pageWidth,
            PrintService printService) {
        if (printable == null || pageWidth == null || printService == null) {
            return;
        }

        Book book = new Book();

        PageFormat pageFormat = getPageFormat(pageWidth);

        book.append(printable, pageFormat);

        PrinterJob job = PrinterJob.getPrinterJob();

        job.setPageable(book);

        try {
            job.setPrintService(printService);
            job.print();
        } catch (Exception e) {
            LOGGER.debug(e.getMessage(), e);
            throw new ApplicationError(e);
        }
    }

    private static PageFormat getPageFormat(
            PageWidth pageWidth) {
        PageFormat pageFormat = new PageFormat();
        pageFormat.setOrientation(PageFormat.PORTRAIT);

        Paper paper = new Paper();
        paper.setSize(getPoint(pageWidth.getWidth()), getPoint(297));
        paper.setImageableArea(0, 0, getPoint(pageWidth.getWidth()), getPoint(297));

        pageFormat.setPaper(paper);

        return pageFormat;
    }

    public static final int getPoint(
            int mm) {
        return (int) (mm / 25.4 * 72);
    }

    /**
     * 获取本地打印机列表
     * 
     * @return 本地打印机列表
     */
    public static Map<String, PrintService> getLocalPrinterMap() {

        Map<String, PrintService> map = new HashMap<>(PrinterJob.lookupPrintServices().length);

        for (PrintService printService : PrinterJob.lookupPrintServices()) {
            map.put(printService.getName(), printService);
        }

        return map;
    }
}
