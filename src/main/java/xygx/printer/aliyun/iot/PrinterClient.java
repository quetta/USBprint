package xygx.printer.aliyun.iot;

import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.print.PrintService;

import org.eclipse.paho.client.mqttv3.IMqttMessageListener;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import com.aliyun.iot.util.ALiyunIotX509TrustManager;
import com.aliyun.iot.util.LogUtil;
import com.aliyun.iot.util.SignUtil;

import net.guerlab.commons.abstractslayout.ApplicationError;
import net.guerlab.commons.random.RandomUtil;
import xygx.printer.helper.PageWidth;
import xygx.printer.helper.PrinterHelper;

public class PrinterClient {

    private String productKey;

    private String deviceName;

    private String secret;

    private PrintService printService;

    private PageWidth pageWidth;

    public PrinterClient(
            PrintService printService,
            String productKey,
            String deviceName,
            String secret,
            PageWidth pageWidth) {
        this.printService = printService;
        this.productKey = productKey;
        this.deviceName = deviceName;
        this.secret = secret;
        this.pageWidth = pageWidth;
    }

    public void start() {
        String clientId = null;
        try {
            clientId = InetAddress.getLocalHost().getHostAddress();
        } catch (Exception e) {
            e.printStackTrace();
            clientId = RandomUtil.nextString(32);
        }

        String t = String.valueOf(System.currentTimeMillis());

        Map<String, String> params = new HashMap<String, String>(4);
        params.put("productKey", productKey);
        params.put("deviceName", deviceName);
        params.put("clientId", clientId);
        params.put("timestamp", t);

        String targetServer = "ssl://" + productKey + ".iot-as-mqtt.cn-shanghai.aliyuncs.com:1883";
        String mqttclientId = clientId + "|securemode=2,signmethod=hmacsha1,timestamp=" + t + "|";
        String mqttUsername = deviceName + "&" + productKey; // mqtt用户名格式
        String mqttPassword = SignUtil.sign(params, secret, "hmacsha1"); // 签名

        try {
            connectMqtt(targetServer, mqttclientId, mqttUsername, mqttPassword, deviceName);
        } catch (Exception e) {
            e.printStackTrace();
            throw new ApplicationError(e.getMessage(), e);
        }
    }

    private SSLSocketFactory createSSLSocket() throws Exception {
        SSLContext context = SSLContext.getInstance("TLSV1.2");
        context.init(null, new TrustManager[] {
                new ALiyunIotX509TrustManager()
        }, null);
        SSLSocketFactory socketFactory = context.getSocketFactory();
        return socketFactory;
    }

    public void connectMqtt(
            String url,
            String clientId,
            String mqttUsername,
            String mqttPassword,
            final String deviceName) throws Exception {
        String subTopic = "/" + productKey + "/" + deviceName + "/print";

        MemoryPersistence persistence = new MemoryPersistence();
        SSLSocketFactory socketFactory = createSSLSocket();
        final MqttClient sampleClient = new MqttClient(url, clientId, persistence);
        MqttConnectOptions connOpts = new MqttConnectOptions();
        connOpts.setMqttVersion(4); // MQTT 3.1.1
        connOpts.setSocketFactory(socketFactory);

        // 设置是否自动重连
        connOpts.setAutomaticReconnect(true);

        // 如果是true，那么清理所有离线消息，即QoS1或者2的所有未接收内容
        connOpts.setCleanSession(true);

        connOpts.setUserName(mqttUsername);
        connOpts.setPassword(mqttPassword.toCharArray());
        connOpts.setKeepAliveInterval(65);

        LogUtil.print(clientId + "进行连接, 目的地: " + url);
        sampleClient.connect(connOpts);

        LogUtil.print(clientId + "连接成功:---");

        sampleClient.subscribe(subTopic, new IMqttMessageListener() {

            @Override
            public void messageArrived(
                    String topic,
                    MqttMessage message) throws Exception {
                String data = new String(message.getPayload(), "UTF-8");

                System.out.println(data);

                PrinterHelper.print(new PrintableImpl(data), pageWidth, printService);
            }
        });

    }
}
