package xygx.printer.entity.order;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 订单商品
 * 
 * @author guer
 *
 */
@ApiModel("订单商品")
public class OrderGoods implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 订单商品ID
     */
    @ApiModelProperty("订单商品ID")
    private Long orderGoodsId;

    /**
     * 订单ID
     */
    @ApiModelProperty("订单ID")
    private Long orderId;

    /**
     * 商品ID
     */
    @ApiModelProperty("商品ID")
    private Long goodsId;

    /**
     * 商品名称
     */
    @ApiModelProperty("商品名称")
    private String goodsName;

    /**
     * 商品价格
     */
    @ApiModelProperty("商品价格")
    private BigDecimal goodsPrice;

    /**
     * 商品数量
     */
    @ApiModelProperty("商品数量")
    private BigDecimal goodsCount;

    /**
     * 现价小计
     */
    @ApiModelProperty("现价小计")
    private BigDecimal amount;

    /**
     * 原价小计
     */
    @ApiModelProperty("原价小计")
    private BigDecimal oldAmount;

    /**
     * 添加时间
     */
    @ApiModelProperty("添加时间")
    private LocalDateTime addTime;

    /**
     * 商品描述
     */
    @ApiModelProperty("商品描述")
    private String goodsContent;

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((orderGoodsId == null) ? 0 : orderGoodsId.hashCode());
        return result;
    }

    @Override
    public boolean equals(
            Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof OrderGoods)) {
            return false;
        }
        OrderGoods other = (OrderGoods) obj;
        if (orderGoodsId == null) {
            if (other.orderGoodsId != null) {
                return false;
            }
        } else if (!orderGoodsId.equals(other.orderGoodsId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("OrderGoods [orderGoodsId=");
        builder.append(orderGoodsId);
        builder.append(", orderId=");
        builder.append(orderId);
        builder.append(", goodsId=");
        builder.append(goodsId);
        builder.append(", goodsName=");
        builder.append(goodsName);
        builder.append(", goodsPrice=");
        builder.append(goodsPrice);
        builder.append(", goodsCount=");
        builder.append(goodsCount);
        builder.append(", amount=");
        builder.append(amount);
        builder.append(", oldAmount=");
        builder.append(oldAmount);
        builder.append(", addTime=");
        builder.append(addTime);
        builder.append(", goodsContent=");
        builder.append(goodsContent);
        builder.append("]");
        return builder.toString();
    }

    /**
     * 返回 订单商品ID
     *
     * @return 订单商品ID
     */
    public Long getOrderGoodsId() {
        return orderGoodsId;
    }

    /**
     * 设置订单商品ID
     *
     * @param orderGoodsId
     *            订单商品ID
     */
    public void setOrderGoodsId(
            Long orderGoodsId) {
        this.orderGoodsId = orderGoodsId;
    }

    /**
     * 返回 订单ID
     *
     * @return 订单ID
     */
    public Long getOrderId() {
        return orderId;
    }

    /**
     * 设置订单ID
     *
     * @param orderId
     *            订单ID
     */
    public void setOrderId(
            Long orderId) {
        this.orderId = orderId;
    }

    /**
     * 返回 商品ID
     *
     * @return 商品ID
     */
    public Long getGoodsId() {
        return goodsId;
    }

    /**
     * 设置商品ID
     *
     * @param goodsId
     *            商品ID
     */
    public void setGoodsId(
            Long goodsId) {
        this.goodsId = goodsId;
    }

    /**
     * 返回 商品名称
     *
     * @return 商品名称
     */
    public String getGoodsName() {
        return goodsName;
    }

    /**
     * 设置商品名称
     *
     * @param goodsName
     *            商品名称
     */
    public void setGoodsName(
            String goodsName) {
        this.goodsName = goodsName;
    }

    /**
     * 返回 商品价格
     *
     * @return 商品价格
     */
    public BigDecimal getGoodsPrice() {
        return goodsPrice;
    }

    /**
     * 设置商品价格
     *
     * @param goodsPrice
     *            商品价格
     */
    public void setGoodsPrice(
            BigDecimal goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    /**
     * 返回 商品数量
     *
     * @return 商品数量
     */
    public BigDecimal getGoodsCount() {
        return goodsCount;
    }

    /**
     * 设置商品数量
     *
     * @param goodsCount
     *            商品数量
     */
    public void setGoodsCount(
            BigDecimal goodsCount) {
        this.goodsCount = goodsCount;
    }

    /**
     * 返回 现价小计
     *
     * @return 现价小计
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * 设置现价小计
     *
     * @param amount
     *            现价小计
     */
    public void setAmount(
            BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * 返回 原价小计
     *
     * @return 原价小计
     */
    public BigDecimal getOldAmount() {
        return oldAmount;
    }

    /**
     * 设置原价小计
     *
     * @param oldAmount
     *            原价小计
     */
    public void setOldAmount(
            BigDecimal oldAmount) {
        this.oldAmount = oldAmount;
    }

    /**
     * 返回 添加时间
     *
     * @return 添加时间
     */
    public LocalDateTime getAddTime() {
        return addTime;
    }

    /**
     * 设置添加时间
     *
     * @param addTime
     *            添加时间
     */
    public void setAddTime(
            LocalDateTime addTime) {
        this.addTime = addTime;
    }

    /**
     * 返回 商品描述
     *
     * @return 商品描述
     */
    public String getGoodsContent() {
        return goodsContent;
    }

    /**
     * 设置商品描述
     *
     * @param goodsContent
     *            商品描述
     */
    public void setGoodsContent(
            String goodsContent) {
        this.goodsContent = goodsContent;
    }
}
