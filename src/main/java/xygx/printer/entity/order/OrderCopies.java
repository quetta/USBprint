package xygx.printer.entity.order;

public class OrderCopies {

    private int copies;

    private Order order;

    /**
     * 返回 copies
     *
     * @return copies
     */
    public int getCopies() {
        return copies;
    }

    /**
     * 设置copies
     *
     * @param copies
     *            copies
     */
    public void setCopies(
            int copies) {
        this.copies = copies;
    }

    /**
     * 返回 order
     *
     * @return order
     */
    public Order getOrder() {
        return order;
    }

    /**
     * 设置order
     *
     * @param order
     *            order
     */
    public void setOrder(
            Order order) {
        this.order = order;
    }

}
