package xygx.printer.aliyun.config;

import org.springframework.stereotype.Component;

@Component
public class AliyunConfig {

    public String productKey;

    /**
     * 返回 productKey
     *
     * @return productKey
     */
    public String getProductKey() {
        return productKey;
    }

    /**
     * 设置productKey
     *
     * @param productKey
     *            productKey
     */
    public void setProductKey(
            String productKey) {
        this.productKey = productKey;
    }
}
