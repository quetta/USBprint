package xygx.printer.helper;

public enum PageWidth {

    MM58(56), MM80(78);

    private int width;

    private PageWidth(
            int width) {
        this.width = width;
    }

    /**
     * 返回 width
     *
     * @return width
     */
    public int getWidth() {
        return width;
    }
}
