package xygx.printer.entity;

import io.swagger.annotations.ApiModelProperty;

public class Result<T> {

    /**
     * 响应状态
     */
    @ApiModelProperty(value = "响应状态", required = true)
    protected boolean status;

    /**
     * 消息
     */
    @ApiModelProperty(value = "响应消息", required = true)
    protected String message;

    /**
     * 数据
     */
    @ApiModelProperty(value = "响应数据", required = false)
    protected T data;

    /**
     * 错误码
     */
    @ApiModelProperty(value = "错误码", required = true)
    protected int errorCode = 0;

    /**
     * 无参构造
     */
    public Result() {
        this(null, null);
    }

    /**
     * 通过设置消息内容来初始化结果集
     *
     * @param message
     *            消息内容
     */
    public Result(
            String message) {
        this(false, message, null);
    }

    /**
     * 通过设置消息内容和数据来初始化结果集
     *
     * @param message
     *            消息内容
     * @param data
     *            数据
     */
    public Result(
            String message,
            T data) {
        this(false, message, data);
    }

    /**
     * 通过设置消息内容和数据来初始化结果集
     *
     * @param message
     *            消息内容
     * @param errorCode
     *            错误码
     */
    public Result(
            String message,
            int errorCode) {
        this(false, message, null, errorCode);
    }

    /**
     * 通过设置响应状态、消息内容和数据来初始化结果集
     *
     * @param status
     *            响应状态
     * @param message
     *            消息内容
     * @param data
     *            数据
     */
    public Result(
            boolean status,
            String message,
            T data) {
        this(status, message, data, 0);
    }

    /**
     * 通过设置响应状态、消息内容和数据来初始化结果集
     *
     * @param status
     *            响应状态
     * @param message
     *            消息内容
     * @param data
     *            数据
     * @param errorCode
     *            错误码
     */
    public Result(
            boolean status,
            String message,
            T data,
            int errorCode) {
        this.status = status;
        this.message = message;
        this.data = data;
        this.errorCode = errorCode;
    }

    /**
     * 获取响应状态
     *
     * @return 响应状态
     */
    public final boolean isStatus() {
        return status;
    }

    /**
     * 设置响应状态
     *
     * @param status
     *            响应状态
     */
    public final void setStatus(
            boolean status) {
        this.status = status;
    }

    /**
     * 获取消息内容
     *
     * @return 消息内容
     */
    public final String getMessage() {
        return message;
    }

    /**
     * 设置消息内容
     *
     * @param message
     *            消息内容
     */
    public final void setMessage(
            String message) {
        this.message = message;
    }

    /**
     * 获取数据
     *
     * @return 数据
     */
    public final T getData() {
        return data;
    }

    /**
     * 设置数据
     *
     * @param data
     *            数据
     */
    public final void setData(
            T data) {
        this.data = data;
    }

    /**
     * 返回错误码
     *
     * @return 错误码
     */
    public int getErrorCode() {
        return errorCode;
    }

    /**
     * 设置错误码
     *
     * @param errorCode
     *            错误码
     */
    public void setErrorCode(
            int errorCode) {
        this.errorCode = errorCode;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Result [status=");
        builder.append(status);
        builder.append(", message=");
        builder.append(message);
        builder.append(", data=");
        builder.append(data);
        builder.append(", errorCode=");
        builder.append(errorCode);
        builder.append("]");
        return builder.toString();
    }
}
