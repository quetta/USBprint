package xygx.printer.config;

import java.time.LocalDate;
import java.time.LocalDateTime;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.alibaba.fastjson.parser.ParserConfig;

import net.guerlab.commons.fastjson.LocalDateDeserializer;
import net.guerlab.commons.fastjson.LocalDateTimeDeserializer;

@Configuration
public class JsonConfig {

    /**
     * 解析配置
     *
     * @return 解析配置
     */
    @Bean
    public ParserConfig parserConfig() {
        ParserConfig parserConfig = ParserConfig.getGlobalInstance();
        parserConfig.putDeserializer(LocalDate.class, LocalDateDeserializer.INSTANCE);
        parserConfig.putDeserializer(LocalDateTime.class, LocalDateTimeDeserializer.INSTANCE);
        return parserConfig;
    }
}
